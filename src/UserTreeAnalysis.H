/**
 * @class UserTreeAnalysis
 * @brief Advanced user analysis
 *
 * This is an example of routine that can be used within user program
 * to perform more advanced analysis and create custom plots
 */
#ifndef __USERTREEANALYSIS_H_
#define __USERTREEANALYSIS_H_

#include "HEPEvent.H"

int UserTreeAnalysis(HEPParticle *mother,HEPParticleList *stableDaughters, int nparams, double *params);

#endif
