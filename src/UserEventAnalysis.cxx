/*
   UserEventAnalysis class implementation
   LC_EventAnalysis, HerwigEventAnalysis and MadGraphEventReader class implementation
*/

#include "UserEventAnalysis.H"

ClassImp(UserEventAnalysis)

UserEventAnalysis::UserEventAnalysis()
{
    printf("creating user event analysis save record...\n");
    save_event_data = new char[4000*120];
    save_event      = new HEPEVTEvent(save_event_data,4000);
    event_name      = "UserEventAnalysis";
}

UserEventAnalysis::~UserEventAnalysis()
{
    printf("destroying user event analysis save record...\n");
    if (save_event) delete save_event;
    if (save_event_data) delete[] save_event_data;
}

void UserEventAnalysis::SaveOriginalEvent(HEPEvent *e)
{
    printf("SAVE ORIGINAL EVENT ... UNIMPLEMENTED\n");
}

void UserEventAnalysis::RestoreOriginalEvent(HEPEvent *e)
{
    printf("RESTORE ORIGINAL EVENT ... UNIMPLEMENTED\n");
}

HEPEvent* UserEventAnalysis::ModifyEvent(HEPEvent *e)
{
    printf("MODIFY EVENT: Nothing done...\n");

    return e;
}


ClassImp(LC_EventAnalysis)

LC_EventAnalysis::LC_EventAnalysis()
{
    event_name = "LC_EventAnalysis";
}

void LC_EventAnalysis::SaveOriginalEvent(HEPEvent *e)
{
    
//    printf("SAVE ORIGINAL EVENT ...\n");    save_event->SetNumOfParticles(1);
    save_event->SetNumOfParticles(1);
    HEPParticle *p1=e->GetParticle(1);
    HEPParticle *p_sav=save_event->GetParticle(1);
    p_sav->Assign(*p1);
//    printf("saved particle 1:\n");
//    save_event->GetParticle(1)->ls();
}

void LC_EventAnalysis::RestoreOriginalEvent(HEPEvent *e)
{
//    printf("RESTORE ORIGINAL EVENT ...\n");
    HEPParticle *p1=e->GetParticle(1);
    HEPParticle *p_sav=save_event->GetParticle(1);
    p1->Assign(*p_sav);
    
//    e->ls();
}

HEPEvent* LC_EventAnalysis::ModifyEvent(HEPEvent *e)
{
    if (!e) return e;

//    printf("MODIFY EVENT\n");
    save_event->SetNumOfParticles(e->GetNumOfParticles());

    // incomming particles...
    HEPParticle *p_1=e->GetParticle(1);
    HEPParticle *p_2=e->GetParticle(2);
    //modified "beam-like particle..."
    HEPParticle *p1=save_event->GetParticle(1);
    HEPParticle *p2=save_event->GetParticle(2);
    
    p1->Assign(*p_1);
    p1->SetPDGId(100);
    p1->SetE(p_1->GetE() + p_2->GetE());
    p1->SetPx(p_2->GetPx());
    p1->SetPy(p_2->GetPy());
    p1->SetPz(p_2->GetPz());
    p1->SetStatus(2); //decays

    p2->Assign(*p_1);
    p2->SetStatus(1);
    p2->SetMother(1);
    p2->SetFirstDaughter(0);
    p2->SetLastDaughter(0);
    

    int first_true_particle=2;
    int idx=3;

    int first_decay_found=0;
    // modified particles ..."
    for (int i=3; i<=e->GetNumOfParticles();i++,idx++) {
    
	HEPParticle *p=e->GetParticle(i);
	HEPParticle *p0=save_event->GetParticle(idx);	
	p0->Assign(*p);
	    //printf("trying...\n");
	    //p->ls();

	if (p->Decays()) {
	    p0->SetStatus(2);
	    
	    if (!first_decay_found) {
		//printf("this is first decay!\n");
		first_decay_found=p0->GetFirstDaughter();
	    }
	}

	if (p->IsHistoryEntry()) {
	    p0->SetStatus(3);
	 } else {
	    //if (first_true_particle)
	    //first_true_particle=i;

	    p0->SetMother(1);	
	 }


    //printf("OK!\n");    
    //printf("Assigned:\n");
    //p0->ls();
    
    }


// now loop again and correct mother-daughter pointers if decays...
    for (int i=2; i<=e->GetNumOfParticles();i++) {
	HEPParticle *p0=save_event->GetParticle(i);
	if (p0->Decays()) {
	    for (int j=p0->GetFirstDaughter(); j<=p0->GetLastDaughter(); j++) {
		if ((j==0 ) || (j<p0->GetFirstDaughter())) {
		    // protect from strange entries...
		    p0->SetStatus(1);		
		} else{
		
		    HEPParticle *d=save_event->GetParticle(j);
		    d->SetMother(i);
		}
	    }
	
	}
    }
/*
// and at the end - add second incoming particle...
    HEPParticle *p2=save_event->GetParticle(idx);
printf("ASSIGN:");
    p_2->ls(); 
    p2->Assign(*p_2);
    p2->SetMother(1);
    p2->SetId(idx);
    p2->SetStatus(1);
printf("LAST:");
    p2->ls();    

    idx++;
*/

// and modify daughter's pointers...
    p1->SetFirstDaughter(first_true_particle);

    if (!first_decay_found) {
//	printf("FIRST DECAY NOT FOUND!!!\n");
	first_decay_found=save_event->GetNumOfParticles()+1;
    }
    p1->SetLastDaughter(first_decay_found-1);
    
    
    
    save_event->SetNumOfParticles(idx-1);

//printf("-------------\n");
//    save_event->ls();


/*    
    
    HEPParticle *p2=save_event->GetParticle(2);
    HEPParticle *p3=save_event->GetParticle(3);
    HEPParticle *p4=save_event->GetParticle(4);
    
    p2->Assign(*(e->GetParticle(2)));
    p3->Assign(*(e->GetParticle(3)));
    p4->Assign(*(e->GetParticle(4)));
    
    p2->SetMother(1); p2->SetStatus(1);
    p3->SetMother(1); p3->SetStatus(1);
    p4->SetMother(1); p4->SetStatus(1);
*/


//    e->ls();

//    return e;

    return save_event;
}


ClassImp(HerwigEventAnalysis)

HerwigEventAnalysis::HerwigEventAnalysis()
{
    printf("creating HerwigEventAnalysis\n");
    event_name = "HerwigEventAnalysis";
}

HEPEvent* HerwigEventAnalysis::ModifyEvent(HEPEvent *e)
{
    //printf("HERWIG Event Analysis modifying event record...\n");

    save_event->SetNumOfParticles(e->GetNumOfParticles());

    for (int i=1; i<=e->GetNumOfParticles();i++) {
	HEPParticle *p_orig=e->GetParticle(i);
	HEPParticle *p=save_event->GetParticle(i);
	p->Assign(*p_orig);
    }
    for (int i=1; i<=e->GetNumOfParticles();i++) {
	HEPParticle *p=save_event->GetParticle(i);

	if ((p->GetPDGId() == 15) ||(p->GetPDGId() == -15)) {
	
	    //printf("OK. Found tau: ");
	    //p->ls();
	
	    int daughter0id=p->GetFirstDaughter();
	    //printf("mother is %i daughter0 is %i\n",motherid,daughter0id);
	    
	    HEPParticle *daughter0=save_event->GetParticle(daughter0id);
		//daughter0->ls();
	    
	    if (daughter0->GetPDGId() == p->GetPDGId() ) {
	    
		//printf("OK! This is my alter-ego!\n");
		p->SetMother2(0);
    		p->SetFirstDaughter(daughter0id);
		p->SetLastDaughter(daughter0id);
		daughter0->SetMother(i);
		daughter0->SetMother2(0);
		daughter0->SetStatus(2);
	    }
	}
    }
    return save_event;
}

ClassImp(MadGraphEventReader)

MadGraphEventReader::MadGraphEventReader( const char *file_name)
{
    if (strlen(file_name)==0) {
	printf("MadGraphEventReader constructor error:\n");
	printf(" you haven't provided file name!\n\n");
	exit(-1);
    
    }

    printf("opening:%s\n",file_name);    
    f=fopen(file_name,"r");
    if (!f) {
	printf("MadGraphEventReader constructor error:\n");
	printf("cannot open file %s\n\n",file_name);
	exit(-2);
    }
    event_name = "MadGraphEventReader";
}

HEPEvent* MadGraphEventReader::ModifyEvent()
{
    return save_event;
}

void MadGraphEventReader::SaveOriginalEvent(HEPEvent *e)
{

}

void MadGraphEventReader::RestoreOriginalEvent(HEPEvent *e)
{

}
		
int MadGraphEventReader::ReadNextEvent()
{
    char line[1024];

    double weight, dummy1, dummy2;
    double px,py,pz,E;
    int evt_no, npart;
    int motherid, daughterid,pdg;
    int result, chars_read, offset;
    
    
    // read all the comment lines.
    // the first valid dataline stays in "line" variable
    while (!feof(f)) {
	if (!fgets(line,1024,f)){
	    return 0;
	}
	
	if (line[0]!='#') break;
    }
    

    result=sscanf(line," %i %i %lf %lf %lf",&evt_no,&npart, &weight,&dummy1,&dummy2);
    if (result < 3) {
	    printf("MadGraphEventReader::ReadNextEvent error:\n");
	    printf("Cannot parse line:\n");
	    printf("%s\n",line);
	    return 0;
    }
    
    printf("Event:%i Weight:%f Nparticles:%i\n",evt_no,weight,npart);
    save_event->SetNumOfParticles(npart);

    // now read particle types (PDG)
    if (!fgets(line,1024,f)){
	    return 0;
    }

    offset=0;
    for (int i=1;i<=npart;i++) {
	result=sscanf(line+offset," %i%n",&pdg,&chars_read);
	printf("reading pdg %i (chars=%i)\n",pdg,chars_read);
	offset+=chars_read;
	if (!result) return 0;
	HEPParticle *p=save_event->GetParticle(i);
	p->SetPDGId(pdg);
    }

    // read mothers...
    if (!fgets(line,1024,f)){
	    return 0;
    }

    offset=0;
    for (int i=1;i<=npart;i++) {
	result=sscanf(line+offset," %i%n",&motherid,&chars_read);
	printf("reading mother %i (chars=%i)\n",motherid,chars_read);
	offset+=chars_read;
	if (!result) return 0;
	HEPParticle *p=save_event->GetParticle(i);
	p->SetMother(motherid);
    }


    // read daughters...
    if (!fgets(line,1024,f)){
	    return 0;
    }

    offset=0;
    for (int i=1;i<=npart;i++) {
	result=sscanf(line+offset," %i%n",&daughterid,&chars_read);
	printf("reading daughter %i (chars=%i)\n",daughterid,chars_read);
	offset+=chars_read;
	if (!result) return 0;
	HEPParticle *p=save_event->GetParticle(i);
	p->SetFirstDaughter(daughterid);
    }

    // read daughters2...
    if (!fgets(line,1024,f)){
	    return 0;
    }

    offset=0;
    for (int i=1;i<=npart;i++) {
	result=sscanf(line+offset," %i%n",&daughterid,&chars_read);
	printf("reading daughter2 %i (chars=%i)\n",daughterid,chars_read);
	offset+=chars_read;
	if (!result) return 0;
	HEPParticle *p=save_event->GetParticle(i);
	p->SetLastDaughter(daughterid);
    }

    // read 2 lines of colour:
    if (!fgets(line,1024,f)){
	    return 0;
    }
    printf("READ COLORLINE %s\n",line);
    if (!fgets(line,1024,f)){
	    return 0;
    }
    printf("READ COLORLINE %s\n",line);

    // read momenta:
    for (int i=1;i<=npart;i++) {
	if (!fgets(line,1024,f)){
	    return 0;
	}
	
	result=sscanf(line,"%lf %lf %lf %lf",&px,&py,&pz,&E);
	if (result<4) return 0;
	HEPParticle *p=save_event->GetParticle(i);
	p->SetPx(px);
	p->SetPy(py);
	p->SetPz(pz);
	p->SetE(E);
    }
    save_event->ls();
    
    return 1;
}
