/**
 * @class TDecayResult
 * @brief Result of hisogram comparison
 *
 * -- Class stored in ROOT file --
 *
 * Stores copies of compared histograms and the difference information
 * Used to create final plots for the booklet
 * One copy of this class is created for each histogram
 */
#ifndef __TDecayResult_H_
#define __TDecayResult_H_
#include "TNamed.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TGaxis.h"
#include "TBrowser.h"

class TDecayResult : public TNamed{
    public:
	TCanvas *c;
	TH1D *h1;
	TH1D *h2;
	TH1D *hdiff;
	TGaxis *axis2;
	double fit_parameter;

	TDecayResult():c(0),h1(0),h2(0),hdiff(0),axis2(0),fit_parameter(0){}
	virtual void Draw(const char *option=0);
	virtual void Browse(TBrowser *b);
    private:



	ClassDef(TDecayResult,1) // Decay mode class



    
};
    
#endif

