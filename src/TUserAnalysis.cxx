/*
   KolmogorovAnalysis implementation

   We're using ROOT build-in comparison as an example of UserAnalysis
*/
#include "TUserAnalysis.H"
#include "TH1.h"

double KolmogorovAnalysis(TH1D *h1, TH1D *h2)
{
    return h1->KolmogorovTest(h2);
}

