#!/bin/sh

echo "This option will overwrite all configuration scripts/makefiles"
echo "and modify the configuration procedure to match LCG setup."
echo "Installation scripts written by Oleg Zenin & Dmitri Konstantinov."
echo ""
echo "You will need autotools version 2.59 or higher."
echo ""
echo "Proceed? (Yes/No)"
read ANSWER

ANSWER=`echo $ANSWER | tr "[:upper:]" "[:lower:]"`

if test "$ANSWER" = "yes" || test "$ANSWER" = "y"; then
	rm -rf ../config* ../make* ../examples-C++/pythia/config* ../examples-C++/pythia/make*
	cp -rf LCGCONFIG/* ../.
	cd ..
	autoreconf --install --force
	cd examples-C++
	autoreconf --install --force
	echo "Done."
else
	echo "Aborted."
fi
