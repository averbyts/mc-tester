{
    //
    // example of more elaborated SETUP.C file:
    //
    // generator description is read from GENSPEC.txt file
    //
    Setup::debug_mode=false;

    // default histogram settings:
    Setup::EVENT=&HEPEVT;
    Setup::SetHistogramDefaults(60, 0.0, 6.0);

    Setup::decay_particle=521;

    FILE *f=fopen("GENSPEC.txt","r");
    
    if (!f) {
	printf("cannot open GENSPEC.txt file");
    } else {
	printf("Reading generator description from GENSPEC.txt file\n");

	// read up to 3 lines of description...
	char *line = new char[512];
	for (int i=0;i<3;i++) {
	    if (feof(f)) break;
	    fgets(line+128*i,127,f);
	    printf("GENSPEC line %i: %s",i+1,line+128*i);
	}
	Setup::gen1_desc_1=line;
	Setup::gen1_desc_2=line+128;
	Setup::gen1_desc_3=line+256;
	fclose(f);
    }

Setup::result1_path="/tmp/ndavidso/mc-tester_evtgenlhc.root";

Setup::SuppressDecay( 111);
//pi0

Setup::SuppressDecay( 311);
//K0

Setup::SuppressDecay( -311);
//K~0

Setup::SuppressDecay( 321);
//K+

Setup::SuppressDecay( -321);
//K-

Setup::SuppressDecay( 411);
//D+

Setup::SuppressDecay( -411);
//D-

Setup::SuppressDecay( 421);
//D0

Setup::SuppressDecay( -421);
//D~0

Setup::SuppressDecay( 431);
//D_s+

Setup::SuppressDecay( -431);
//D_s-

Setup::SuppressDecay( 511);
//B0

Setup::SuppressDecay( -511);
//B~0

Setup::SuppressDecay( 531);
//B_s0

Setup::SuppressDecay( -531);
//B_s~0

Setup::SuppressDecay( 541);
//B_c+

Setup::SuppressDecay( -541);
//B_c-

Setup::SuppressDecay( 221);
//eta

Setup::SuppressDecay( 331);
//eta'

Setup::SuppressDecay( 441);
//eta_c

Setup::SuppressDecay( 551);
//eta_b

Setup::SuppressDecay( 661);
//eta_t

Setup::SuppressDecay( 130);
//K_L0

Setup::SuppressDecay( 310);
//K_S0

Setup::SuppressDecay( -130);
//K_L~0

Setup::SuppressDecay( -310);
//K_S~0

Setup::SuppressDecay( 213);
//rho+

Setup::SuppressDecay( -213);
//rho-

Setup::SuppressDecay( 313);
//K*0

Setup::SuppressDecay( -313);
//K*~0

Setup::SuppressDecay( 323);
//K*+

Setup::SuppressDecay( -323);
//K*-

Setup::SuppressDecay( 413);
//D*+

Setup::SuppressDecay( -413);
//D*-

Setup::SuppressDecay( 423);
//D*0

Setup::SuppressDecay( -423);
//D*~0

Setup::SuppressDecay( 433);
//D*_s+

Setup::SuppressDecay( -433);
//D*_s-

Setup::SuppressDecay( 513);
//B*0

Setup::SuppressDecay( -513);
//B*~0

Setup::SuppressDecay( 523);
//B*+

Setup::SuppressDecay( -523);
//B*-

Setup::SuppressDecay( 533);
//B*_s0

Setup::SuppressDecay( -533);
//B*_s~0

Setup::SuppressDecay( 543);
//B*_c+

Setup::SuppressDecay( -543);
//B*_c-

Setup::SuppressDecay( 113);
//rho0

Setup::SuppressDecay( 223);
//omega

Setup::SuppressDecay( 333);
//phi

Setup::SuppressDecay( 443);
//J/psi

Setup::SuppressDecay( 553);
//Upsilon

Setup::SuppressDecay( 663);
//Theta

Setup::SuppressDecay( 10213);
//b_1+

Setup::SuppressDecay( -10213);
//b_1-

Setup::SuppressDecay( 10313);
//K_10

Setup::SuppressDecay( -10313);
//K_1~0

Setup::SuppressDecay( 10323);
//K_1+

Setup::SuppressDecay( -10323);
//K_1-

Setup::SuppressDecay( 10413);
//D_1+

Setup::SuppressDecay( -10413);
//D_1-

Setup::SuppressDecay( 10423);
//D_10

Setup::SuppressDecay( -10423);
//D_1~0

Setup::SuppressDecay( 10433);
//D_1s+

Setup::SuppressDecay( -10433);
//D_1s-

Setup::SuppressDecay( 10113);
//b_10

Setup::SuppressDecay( 10223);
//h_10

Setup::SuppressDecay( 10333);
//h'_10

Setup::SuppressDecay( 10443);
//h_1c0

Setup::SuppressDecay( 10211);
//a_0+

Setup::SuppressDecay( -10211);
//a_0-

Setup::SuppressDecay( 10311);
//K*_00

Setup::SuppressDecay( -10311);
//K*_0~0

Setup::SuppressDecay( 10321);
//K*_0+

Setup::SuppressDecay( -10321);
//K*_0-

Setup::SuppressDecay( 10411);
//D*_0+

Setup::SuppressDecay( -10411);
//D*_0-

Setup::SuppressDecay( 10421);
//D*_00

Setup::SuppressDecay( -10421);
//D*_0~0

Setup::SuppressDecay( 10431);
//D*_0s+

Setup::SuppressDecay( -10431);
//D*_0s-

Setup::SuppressDecay( 10111);
//a_00

Setup::SuppressDecay( 10221);
//f_00

Setup::SuppressDecay( 10331);
//f'_00

Setup::SuppressDecay( 10441);
//chi_0c0

Setup::SuppressDecay( 20213);
//a_1+

Setup::SuppressDecay( -20213);
//a_1-

Setup::SuppressDecay( 20313);
//K*_10

Setup::SuppressDecay( -20313);
//K*_1~0

Setup::SuppressDecay( 20323);
//K*_1+

Setup::SuppressDecay( -20323);
//K*_1-

Setup::SuppressDecay( 20413);
//D*_1+

Setup::SuppressDecay( -20413);
//D*_1-

Setup::SuppressDecay( 20423);
//D*_10

Setup::SuppressDecay( -20423);
//D*_1~0

Setup::SuppressDecay( 20433);
//D*_1s+

Setup::SuppressDecay( -20433);
//D*_1s-

Setup::SuppressDecay( 20113);
//a_10

};
