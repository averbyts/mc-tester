<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
  <title>MC-TESTER</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <meta name="GENERATOR" content="Quanta Plus">
</head>
<body bgcolor="white" text="black" link="red" alink="green" vlink="magenta">
 <h1> <font color="red">MC-TESTER</font></h1>
 <h2><font color="darkblue">a universal tool for comparisons of Monte Carlo predictions in High Energy Physics</font></h2>

 <br>
 <hr>
 <br>
 
AUTHORS:<BR>
<ul>
<li> <a href="mailto:Piotr.Golonka@CERN.CH">Piotr Golonka</a> (1,2,3)</li>
<li> <a href="mailto:tomek@truth.phys.us.edu.pl">Tomasz Pierzchala</a> (4)</li>
<li> <a href="mailto:Zbigniew.Was@CERN.CH">Zbigniew Was</a> (2,3)</li>
</ul>			    

<font size="-2">
(1) <a href="http://www.ftj.agh.edu.pl/wfitj/english/index.htm">Faculty of Physics and Nuclear Techniques</a>,
    <a href="http://www.agh.edu.pl/index_e.html">AGH University of Science and Technology</a>, Krakow, Poland;<BR>
(2) <a href="http://www.ifj.edu.pl/en/index.html">Institute of Nuclear Physics</a>, Krakow, Poland;<BR>
(3) <a href="http://www.cern.ch">CERN</a>, European Organization for Nuclear Research, Geneva, Switzerland;<BR>
(4) <a href="http://www.phys.us.edu.pl">Institute of Physics</a>, <a href="http://www.us.edu.pl">University of Silesia</a>, Katowice, Poland;<BR>

</font>				
<HR>
<H2> NEWS:</H2>

<i> 30 September 2003</i><br>
<font size=+2 color="green"> MC-TESTER 1.112 released</font><br>
<font size=+1 color="blue"><B>This release is recommended for use! </B></font>
<BR>
<BR>
Some errors have been detected and corrected, new functionality has been added. <BR>
Please refer to  <a href="doc/CHANGELOG">CHANGELOG</a> for details.<BR>

<BR>
<BR>
<font size=+2 color="green"> MC-TESTER 1.1p1 released</font><br>
<font size=+1 color="blue"><B>This is stable release 1.1 with minor corrections</B></font>
<BR>
<HR>
<BR>

    <font color="darkcyan"><I><B>NOTE:</B></I></font> We are interested in the proposals of the C++
    event record standard. Unfortunately we cannot promise the implementation of MC-TESTER/HEPEvent
    interface for new standards in the short timescale.
    <BR>
    <font color="darkcyan"><I><B>NOTE:</B></I></font> Should you encounter any problems in installation
    of the MC-TESTER tool, do not hesitate to contact  <a href="mailto:Piotr.Golonka@CERN.CH"> Piotr Golonka</a>.

<BR>
<HR>
<BR>
 <ul>
   <li> <font size=+2>Download:</font>
        <ul>

	<li><font color="darkcyan"><I><B>NEW!</B></I></font>
	MC-Tester version 1.112  <font color="blue">[DEVELOPMENT/<B>RECOMMENDED</B>]</font>: (30 September 2003):<BR>
	<a href="MC-TESTER-1.112.tar.gz">MC-TESTER-1.112.tar.gz</a>
	</li>
	<BR>
	<li>
	MC-Tester version 1.1p1  <font color="blue">[OLDER/STABLE]</font>: (30 September 2003):<BR>
	<a href="MC-TESTER-1.1p1.tar.gz">MC-TESTER-1.1p1.tar.gz</a>
	</li>
        <li> MC-Tester version 1.0 <font color="blue">[OUTDATED]</font>: (1 October 2002): 
	<a href="MC-TESTER-1.0.tgz">MC-TESTER-1.0.tgz</a> 
	</li>
	<BR>
	<li> 
	<a href="http://root.cern.ch/root/Availability.html">ROOT download</a> page 
	</li>
	<BR>
	<li><font color="darkcyan"><I><B>NOTE!</B></I></font> Upon request we may provide a 
	huge, ready-to-use package with all libraries (MC-TESTER and ROOT) properly installed
	for certain Linux version (i.e. RedHat 6.1). 
	The timescale for preparation of such package would be one week.</li>
	<BR>	
        </ul>
   </li>

<br>
<HR>
<br>
  <li> <font size=+2>Documentation:</font>
    <ul> 
    <li> 
    	 <font color="blue" size="+1"><B><i>MC-TESTER a universal tool for comparisons of Monte Carlo predictions in High Energy Physics</B></i></font><br> 
	<ul>
	<br>
	<li><font color="darkcyan"><I><B>NEW!</B></I></font> Document for <B>version 1.1</B> 
	    <a href="http://authors.elsevier.com/sd/article/S0010465503004661">published</a> in
	 </i>Computer Physics Communications</i>, Vol 157/1 pp 39-62. <BR>
	<br>
	<li> reference for citation: <BR>
	P.Golonka, T.Pierzchala, and Z.Was, <i>Comput. Phys. Commun.</i> <b>157</b>(2004)1, pp 39-62 <BR>
	doi:<a href="http://dx.doi.org/10.1016/S0010-4655(03)00466-1">10.1016/S0010-4655(03)00466-1</a> <BR>
	</li>
	<br>	
	<li>Preprint versions:</li>
	    <ul>

	    <li>  LANL: hep-ph/0210252: (version 1.1)
		<a href="http://arXiv.org/abs/hep-ph/0210252">[Abstract]</a> , 
		<a href="http://arXiv.org/ps/hep-ph/0210252">[PostScript]</a> , 
		<a href="http://arXiv.org/e-print/hep-ph/0210252">[other formats]</a> , 
		<a href="doc/0210252v2.ps.gz">[local PostScript copy]</a> 
	    </li>
	    <li> <a href="http://flc25.desy.de/lcnotes">Linear Collider note</a> LC-TOOL-2003-004 (version 1.1)
		 <a href="http://flc25.desy.de/lcnotes/notes/LC-TOOL-2003-004.pdf">[PDF]</a></li>
	    </li>

	    <li>  CERN-TH/2002-271 (version 1.0) </li>
	    <li>  TP-USl-01/02 (version 1.0) </li>
	    </ul>	
	</ul>
    <br>
    <li> Main <a href="README">README</a> file</li>
    <li> <a href="doc/CHANGELOG">CHANGELOG</a> </li>
    <li> <a href="http://root.cern.ch">ROOT</a> <a href="http://root.cern.ch/root/EnvVars.html">instalation</a> page<BR>
	<a href="doc/ROOT_INSTALL">ROOT instalation howto</a> </B></I>
	    including explanation of use on the CERN/afs version of root</li>
    <li> Description of <a href="doc/README.SETUP">SETUP</a> parameters.</li>
    <li> Description of <a href="doc/README.SETUP.F77">F77 SETUP interface</a>.</li>
    <li> Description of porting MC-Tester to <a href="doc/README.OTHERS">other generators</a>.</li>
    <li> Description of <a href="doc/USER-TESTS">user-defined tests for histogram analysis</a></li>
    <BR>
    <li> Presentations:
	<ul>
	<li><a href="Prague2002"> ECFA-DESY Linear Collider Workshop, Prague, November 2002</a>  </li>
	<li> <a href="http://www.nikhef.nl/ecfa-desy/start.html">ECFA-DESY Linear Collider Workshop, 1-4 April 2003 </a>
        	<a href="http://www.nikhef.nl/ecfa-desy/ECspecific/Program/programoverview.htm#APR3rd1D"> => Generators:</a><BR>
	        <a href="http://www.nikhef.nl/ecfa-desy/ECspecific/Program/Presentations/April-3/Par-1/1D/jadach-s.pdf">
		"MC-TESTER for e+ e- -> 6f" </a>
	</li>

	<li> <a href="http://agenda.cern.ch/fullAgenda.php?ida=a03583"> Higgs-WG meeting, CERN, May 12, 2003 </a> <a href="http://agenda.cern.ch/askArchive.php?base=agenda&categ=a03583&id=a03583s1t11/transparencies">[TRANSPARENCIES]</a> </li>
	<li> <a href="http://agenda.cern.ch/displayLevel.php?fid=152"> CERN Workshop: Monte Carlo tools for the LHC:</a>
	    <ul>
	    <li> <a href="http://agenda.cern.ch/fullAgenda.php?ida=a031457#s1"> Matrix Element tools, 9 July 2003 </a> <a href="http://agenda.cern.ch/askArchive.php?base=agenda&categ=a031457&id=a031457s1t6/transparencies">[TRANSPARENCIES]</a> </li>
	    <li> <a href="http://agenda.cern.ch/fullAgenda.php?ida=a031540#s3"> Decays/Packages, 24 July 2003 </a></li>
	    <li> <a href="http://agenda.cern.ch/fullAgenda.php?ida=a031541#s2"> Tuning and validation tools, 30 July 2003 </li>
	    </ul>
	</li>
	
	</ul>	
    </li>
    </ul>
   </li>
<BR>
<HR>

<BR>
    <li> <font size=+2>Final comment:</font> <BR>
    <BR>
    We are aware that you will find the ways to use the package, we have never thought about.<BR>
    This may expose bugs we have never encountered. <BR>
    Any of your comments on problems or suggestions of new features are welcome.<BR>
    Please direct all your comments to <a href="mailto:Piotr.Golonka@CERN.CH">Piotr Golonka@CERN.CH</a>.
    <BR>
    </li>
    <BR>

    <li> view MC-TESTER <a href="http://mc-tester.blogspot.com">BLOG</a> </li>
   
</ul>

<br>

<hr>
 <font size="-3">
 Last Modified: 21 January 2004<br>
 by <a href="mailto:Piotr.Golonka@CERN.CH"> Piotr Golonka</a>
 </font>

</html>