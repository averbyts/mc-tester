/*! \page examples_evtgenlhc Notes on Generation Step with EvtGenLHC (including example)

  \sa examples-F77/evtgenlhc directory

This page describes the example in examples-F77/evtgenlhc for running MC-TESTER
with EvtGen/EvtGenLHC using the HEPEVT event record format.

  \section running_evtgen Running EvtGen
  
EvtGen is a B decay Monte-Carlo Program by David Lange and Anders Ryd. Although I did not test it, I believe running MC-TESTER with it should be quite straight forward. EvtGen has an internal data structure which contains the event information (EvtStdHep). This structure is a C++ representation of StdHep. When an event is generated with the EvtGen::generateEvent() method, the contents of EvtStdHep is dumped to StdHep. The StdHep event record standard is built on the HEPEVT common block (to my understanding). So once the record is in this format it is readable by MC-TESTER. The only configuration for MC-TESTER should be to set the record type to HEPEVT with Setup::EVENT=&HEPEVT. The MC-TESTER methods MC_Initialize(), MC_Analyze(int pgd_code) and MC_Finalize() should be called in the main loop.

<hr>

  \section running_evtgenlhc Running EvtGenLHC


EvtGenLHC is a version of EvtGen installed on afs at CERN by the GENSER LCG group. Is it a little different to the version of EvtGen from David Lange and Anders Ryd because it's been modified for pp collisions at 14 TeV and because it is used in environments where HepMC handles event records rather than StdHep. In particular, I found that the EvtGen::generateEvent() methods had been labelled as obsolete and the line of code in this method which dumped the event record to StdHep had been removed. Perhaps this is because in the environment of the LHC experiments, event records are never stored as HEPEVT or StdHep. I looked at how ATLAS uses EvtGen (It uses EvtGen currently, not EvtGenLHC). It basically looks for Bs inside a HepMC event. It then generates a decay with EvtGen and copies the new information from EvtStdHep to HepMC using a simple interface which is part of the ATLAS code.

To get EvtGenLHC running with MC-TESTER I copy the StdEvtGen event record into StdHep in the main method. To do this I needed to use the StdHep libraries. (I included stdhep.h from /cern/pro/include/stdhep). EvtGen's event record class, EvtStdHep, follows the HEPEVT common block structure so it was simple to copying each element of the data structure. For example if we have an instance of EvtStdHep called "evtstdhep", the number of particles can be copied to "hepevt_", the HEPEVT block using:

\verbatim hepevt_.nhep = evtstdhep.getNPart(); \endverbatim

To see how the other fields (such as mother/daughter points and 4 momentum) were copied please look at 
evtgen_test.cc from the evtgenlhc example.

<hr>

 \section example_evtgen Example

This example is in MC-TESTER-1.21 (along with some bug fixes described on our <a href="https://savannah.cern.ch/projects/mc-tester/"> savannah bug tracking page</a>).

Here is an example of the code and output for running MC-TESTER on the B-decay package EvtGenLHC (version 5.15) as described above. 500,000 B- decays were compared to B- decays in PYTHIA 8.1. For these results I linked with clhep 1.9.3.1, photos 215.5, pythia 6.227.2 in /afs/cern.ch/sw/lcg/external/

Output Files Produced by MC-TESTER:
- pdf comparison booklet - <a href="http://mc-tester.web.cern.ch/MC-TESTER/mc-tester_results/evtgenlhc_vs_pythia_8.pdf">evtgenlhc_vs_pythia_8.pdf</a>
- root file from generation step of EvtGenLHC - <a href="http://www.ph.unimelb.edu.au/~ndavidson/mc_tester/EvtGen_files/mc-tester_evtgenlhc.root">mc-tester_evtgenlhc.root</a>
- root file from generation step of PYTHIA 8.1 - <a href="http://www.ph.unimelb.edu.au/~ndavidson/mc_tester/EvtGen_files/mc-tester_pythia_8_B.root">mc-tester_pythia_8_B.root</a>

Input Files - all in examples-F77/evtgenlhc:
- code with main method to generate B-decays in EvtGenLHC - evtgen_test.cc
- MC-TESTER configuration file - SETUP.C
- Makefile
- DECAY.DEC
- evt.pdl 


*/
