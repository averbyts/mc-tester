/** 

 @mainpage MC-TESTER-1.24 
 @brief Description of MC-TESTER-1.24

  @authors Piotr Golonka, Tomasz Pierzchala, Zbigniew Was, Nadia Davidson, Tomasz Przedzinski

 @section intro Introduction

Theoretical predictions in high energy physics are often provided in the form of Monte Carlo generators. Comparisons of predictions from different programs and/or different initialization set-ups are often necessary. MC-TESTER can be used for such tests of decays of intermediate states (particles or resonances) in semi-automated way.
Our test consists of two steps. Different Monte Carlo programs are run: events with decays of a chosen particle are searched, decay trees are analyzed and appropriate information is stored. Then, at the analysis step, a list of all found decay modes is defined and branching ratios are calculated for both runs. For each plot a measure of the difference of the distributions is calculated and its maximal value over all histograms for each decay channel is printed in a summary table.
As an example of MC-TESTER application, we include a test with the tau lepton decay Monte Carlo generators, TAUOLA and PYTHIA. The HEPEVT (or LUJETS) common block is used as a source of information on the generated events. This 
new version of MC-TESTER now also allows testing of Monte Calo generators with event records in C++ HepMC format.
An example is provided with tau decays analyzed from PYTHIA 8.1.

 @section install Installation

  In order to compile MC-TESTER:

  - Execute './configure' with additional command line options:
    - '--with-HepMC=<path> ' provides the path to HepMC installation directory. One can set HEPMCLOCATION variable instead of using this directive.
    - '--prefix=<path>' provides the installation path. The 'include' and 'lib' directories will be copied there if 'make install' is executed later. If none has been provided, the default directory for installation is '/usr/local'.
  - Execute 'make'
  - Optionally, execute 'make install' to copy files to the directory provided during configuration.

 If MC-TESTER will be used along with HepMC event record, the path to HepMC installation must be provided during configuration step.
After compiling the MC-TESTER, the '/lib' and '/include' directories will contain the appropriate library and include files. 

<hr>

 @section changes Changelog

 - MC-TESTER-1.24: \ref configuration_scripts
 - MC-TESTER-1.22: \ref new_graphing_options
 - MC-TESTER-1.21: \ref examples_eventgenlhc
 - MC-TESTER-1.2:  \ref examples_C

 @section pages Documentation

- Please visit the official <A HREF="http://cern.ch/MC-TESTER/">MC-TESTER</A> website
- For information about the installation, generation step - with fortran Monte Carlo generators,  analysis
   and configuration of the tool, please see "MC-TESTER a universal tool for comparisons of Monte Carlo predictions in High Energy Physics, P.Golonka, T.Pierzchala, and Z.Was, Comput. Phys. Commun. 157(2004)1, pp 39-62" 
<A href="http://www.sciencedirect.com/science?_ob=ArticleURL&_udi=B6TJ5-4B85618-1&_user=107896&_rdoc=1&_fmt=&_orig=search&_sort=d&view=c&_acct=C000008398&_version=1&_urlVersion=0&_userid=107896&md5=78b13dd879782b5191972f1bddffbe17">doi:10.1016/S0010-4655(03)00466-1</a> (or the <a href="http://arxiv.org/abs/hep-ph/0210252">hep-ph/0210252 pre-print</a>)

<hr>

 @section projects Projects that made use of MC-TESTER

- <a href="http://piters.web.cern.ch/piters/MC/PHOTOS-MCTESTER/">
Tests of PHOTOS Hard Bremsstrahlung</a> documented in <a href="http://www.springerlink.com/content/f5438774n0m44l17/">doi:10.1140/epjc/s2005-02396-4</a> (or see the <a href="http://arxiv.org/abs/hep-ph/0506026">hep-ph/0506026 pre-print</a>)

*/
