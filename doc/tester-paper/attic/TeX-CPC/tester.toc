\contentsline {section}{\numberline {1}Introduction}{4}
\contentsline {section}{\numberline {2}Installation and generation step}{5}
\contentsline {section}{\numberline {3}Analysis}{6}
\contentsline {subsection}{\numberline {3.1}Description of a single plot}{7}
\contentsline {section}{\numberline {4}{\tt Shape Difference Parameter} calculation algorithms}{7}
\contentsline {subsection}{\numberline {4.1}How to set a particular test}{10}
\contentsline {subsection}{\numberline {4.2}{\tt MCTest01} $-$ exclusive surface }{11}
\contentsline {subsubsection}{\numberline {4.2.1}Detailed definition}{11}
\contentsline {subsubsection}{\numberline {4.2.2}Results and interpretation of {\tt MCTest01} }{11}
\contentsline {subsection}{\numberline {4.3}{\tt MCTest02} $-$ non-uniformity of the histograms ratio}{14}
\contentsline {subsubsection}{\numberline {4.3.1}Detailed definition}{14}
\contentsline {subsubsection}{\numberline {4.3.2}Results and interpretation of {\tt MCTest02}}{15}
\contentsline {subsection}{\numberline {4.4}{\tt MCTest03}$-$ Kolmogorov compatibility test}{16}
\contentsline {section}{\numberline {5}Package organization}{16}
\contentsline {subsection}{\numberline {5.1}Directory tree}{16}
\contentsline {subsection}{\numberline {5.2}Libraries}{17}
\contentsline {subsection}{\numberline {5.3}Format and syntax of the {\tt SETUP.C} file}{17}
\contentsline {subsection}{\numberline {5.4}User-defined {\tt Shape Difference Parameter} algorithms}{19}
\contentsline {subsection}{\numberline {5.5}How to make {\tt MC-TESTER} run with other generators}{19}
\contentsline {subsubsection}{\numberline {5.5.1}The case of {\tt F77} }{19}
\contentsline {subsubsection}{\numberline {5.5.2}The case of {\tt C++} }{20}
\contentsline {section}{\numberline {6}Outlook}{21}
\contentsline {subsection}{\numberline {6.1}Recent updates and extensions}{22}
\contentsline {section}{\numberline {A}Appendix: {\tt MC-TESTER} setup and input parameters}{24}
\contentsline {subsection}{\numberline {A.1}Format and use of the {\tt SETUP.C} file}{24}
\contentsline {subsection}{\numberline {A.2}Definition of parameters in the {\tt SETUP.C} file}{24}
\contentsline {subsubsection}{\numberline {A.2.1}Setup::decay\_particle }{24}
\contentsline {subsubsection}{\numberline {A.2.2}Setup::EVENT }{24}
\contentsline {subsubsection}{\numberline {A.2.3}Setup::stage }{25}
\contentsline {subsubsection}{\numberline {A.2.4}Setup::gen1\_desc\_1 , Setup::gen1\_desc\_2, Setup::gen1\_desc\_3}{25}
\contentsline {subsubsection}{\numberline {A.2.5}Setup::gen2\_desc\_1, Setup::gen2\_desc\_2, Setup::gen2\_desc\_3}{26}
\contentsline {subsubsection}{\numberline {A.2.6}Setup::result1\_path}{26}
\contentsline {subsubsection}{\numberline {A.2.7}Setup::result2\_path}{26}
\contentsline {subsubsection}{\numberline {A.2.8}Setup::order\_matters}{26}
\contentsline {subsubsection}{\numberline {A.2.9}Setup::nbins}{27}
\contentsline {subsubsection}{\numberline {A.2.10}Setup::bin\_min}{27}
\contentsline {subsubsection}{\numberline {A.2.11}Setup::bin\_max}{28}
\contentsline {subsubsection}{\numberline {A.2.12}Setup::SetHistogramDefaults(int nbins, double min\_bin, double max\_bin);}{28}
\contentsline {subsubsection}{\numberline {A.2.13}Setup::gen1\_path}{28}
\contentsline {subsubsection}{\numberline {A.2.14}Setup::gen2\_path}{29}
\contentsline {subsubsection}{\numberline {A.2.15}Setup::user\_analysis}{29}
\contentsline {subsubsection}{\numberline {A.2.16}Setup::user\_event\_analysis}{29}
\contentsline {subsubsection}{\numberline {A.2.17}Setup::SuppressDecay(int pdg);}{29}
\contentsline {subsection}{\numberline {A.3} {\tt F77} interface of {\tt MC-TESTER}.}{30}
\contentsline {subsubsection}{\numberline {A.3.1}\texttt {SUBROUTINE MCSETUP( WHAT, VALUE) }}{30}
\contentsline {subsubsection}{\numberline {A.3.2}\texttt {SUBROUTINE MCSETUPHBINS(VALUE) }}{30}
\contentsline {subsubsection}{\numberline {A.3.3}\texttt {SUBROUTINE MCSETUPHMIN(VALUE) }}{30}
\contentsline {subsubsection}{\numberline {A.3.4}\texttt {SUBROUTINE MCSETUPHMAX(VALUE) }}{31}
\contentsline {subsubsection}{\numberline {A.3.5}\texttt {SUBROUTINE MCSETUPHIST(NBODY,NHIST,NBINS,MINBIN,MAXBIN) }}{31}
