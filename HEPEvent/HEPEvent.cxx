/*
   HEPEvent class implementation

   AUTHOR:      Piotr Golonka 
   LAST UPDATE: 2000-01-17
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "HEPEvent.H"
#ifdef _USE_ROOT_
ClassImp(HEPEvent)
#endif

// abstract class for HEP event
HEPEvent::HEPEvent()
{
  // default constructor - dummy.
}
HEPEvent::~HEPEvent()
{
  // default destructor - dummy.
}

void HEPEvent::ls(const char *option)
{
    if ( (option!=0) && (strcmp(option,"FINAL")==0)  ) {
       for (int i=1; i<=GetNumOfParticles(); i++) {
	   HEPParticle *p=GetParticle(i);
	   if ( (p!=0) && (p->GetStatus()==1) ) p->ls();
	   }
    
    } else {
    
       for (int i=1; i<=GetNumOfParticles(); i++) {
	   HEPParticle *p=GetParticle(i);
	   if (p) p->ls(option);
	   }
   }
}

HEPParticleList* HEPEvent::FindParticle(int pdg, HEPParticleList *list)
{
    // generic version, uses GetPDGId();
    //
    // if list is not provided, it is created
    
    if (!list) list=new HEPParticleList();
    
    for (int i=1; i<=GetNumOfParticles();i++) {
    
	HEPParticle *p=GetParticle(i);
	if (p->GetPDGId()==pdg) list->push_back(p);
    }
    return list;
}

#ifdef _USE_ROOT_
void HEPEvent::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility - dummy
}
#endif
