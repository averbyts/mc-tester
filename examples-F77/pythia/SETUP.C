{
    //
    // example of more elaborated SETUP.C file:
    //
    // generator description is read from GENSPEC.txt file
    //

    FILE *f=fopen("GENSPEC.txt","r");
    
    if (!f) {
	printf("cannot open GENSPEC.txt file");
    } else {
	printf("Reading generator description from GENSPEC.txt file\n");

	// read up to 3 lines of description...
	char *line = new char[512];
	for (int i=0;i<3;i++) {
	    if (feof(f)) break;
	    fgets(line+128*i,127,f);
	    printf("GENSPEC line %i: %s",i+1,line+128*i);
	}
	Setup::gen1_desc_1=line;
	Setup::gen1_desc_2=line+128;
	Setup::gen1_desc_3=line+256;
	fclose(f);
    }



   Setup::SuppressDecay(111); // suppress pi0 decays
};
